# Whois?
We're a community of software 200+ devs on a mission We want to make Open Banking a reality a in South Africa: by building open source tech with [Investec Programmable Banking](https://www.offerzen.com/community/investec/). We meet up every week to share what we learn.

# Find out more about Programmable Banking

To find out more, check out these links, or get in touch by emailing @benblaine at [community-investec@offerzen.com](mailto:community-investec@offerzen.com)

- [API Docs](https://developer.investec.com/programmable-banking/#programmable-banking)
- [Ask the Community](https://community.offerzen.com/)
- [Blog Posts](https://community.offerzen.com/c/blog-posts/8)
- [Programmable Businesses Banking](https://docs.google.com/presentation/d/e/2PACX-1vQJFvudepzE4DhSiISyx7TcWdu0R35bCwsRbcVjkvLziy5JLRwfWT3vnMNds7wcCmBxBye021zj7rIv/pub?start=false&loop=false&delayms=3000&slide=id.g91f766fb9b_0_1278)
- [Community Challenges](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/blob/master/community_challenges.md)
- [Background on OfferZen + Investec](https://www.offerzen.com/blog/building-a-programmable-bank-account-for-developers-part-2)
- [Programmable Banking Website](https://www.offerzen.com/community/investec/)
- [Platform Feedback](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/issues-and-ideas)
- [Terms and Condiditions](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/raw/master/investec-docs/investec-programmable-banking-terms-and-conditions)

# Open Source Projects

This is an index of open source code and demos of apps and systems built by the Programmable Banking Dev Community.

`Let's make Open Banking a reality by building, sharing reusable software and connecting!`

If you're looking for something to build, or want to challenge the community to build something. The intention with [community challenges](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/blob/master/community_challenges.md) is to incentivise the creation of "building blocks" in the form of reusable libraries, utilities and code snippets. This should reduce the effort required for others in the community to build personal and business projects.

## Built with Investec Business Banking
| Open Source Repo | Description | Tech Stack | Author |
| - | - | - | - |
| [Team Spend Tracker](https://github.com/Offerzen/roz) | Financial recon for teams using who use Investec Business Banking and the Open API.  | *Ruby on Rails, PostgreSQL, Heroku* | *[@OfferZen](https://github.com/Offerzen/investec_open_api)* |

## Built with Investec Open API

| Open Source Repo | Description | Tech Stack | Author |
| - | - | - | - |
| [Investec Bank GraphQL](https://github.com/naartjie/investec.graphql) | GraphQL API for Investec Open Banking | *F#, .Net Core, GraphQL* | [@naartjie](https://github.com/naartjie/investec.graphql) |
| [Investec to Sage One](https://github.com/imraanparker/investec-to-sage-one) | Sync Investec transactions with Sage One accounting. | *FastAPI, Sage API, Python, Heroku, Python Investec OpenAPI Wrapper* | [@imraanp](https://github.com/imraanparker/investec-to-sage-one) |
| [SparaBox](https://github.com/RendaniDau/Sparabox) and [SparaBoxFlutter](https://github.com/RendaniDau/SparaBoxFlutter) | Personal budget app to explore the potentials of programmable banking. | *Spring Boot, Keycloak, Flutter, Android* | [@RendaniDau](https://github.com/RendaniDau) |
| [investec-openbanking-dotnet](https://github.com/dalion619/investec-openbanking-dotnet) | C# Open API wrapper.  | *C#* | @dalion619 |
| [Java Wrapper for Investec Programmable Banking Open API](https://gitlab.com/vchegwidden/investec-openbanking-java) | Java Open API wrapper.  | *Java* | @vchegwidden |
| [investec-openbanking-python](https://gitlab.com/vchegwidden/investec-openbanking-python) | Python Open API Wrapper. | *Python* | @vchegwidden |
| [investec-openapi](https://github.com/barrymichaeldoyle/investec-openapi) | JavaScript Open API Wrapper. | *JavaScript/TypeScript* | @barrymichaeldoyle |
| [openbanking-investec-dashboard](https://github.com/pivendren/openbanking-investec-dashboard) | Component based dashboard configurable by user. ([slides](https://docs.google.com/presentation/d/1yP8lLos60CFcL3dhWuKQf_u6_FRtzdoGEA5iS1Xv3Qo/edit#slide=id.g7757fe7c3f_0_105)), ([demo](https://www.youtube.com/watch?v=F7qDvejZs9o&amp;t=1)) | *Blazor WebAssembly, Entity Framework, .NET Core, ML.NET, SQL DB, Azure Functions* | [@pivendren](https://github.com/pivendren) |
| [Investec Open Banking CLI](https://github.com/adrianhopebailie/investec) | An interactive CLI application for communicating with the Investec Open Banking APIs. Built with [Deno](https://deno.land/). | *Deno, TS* | [@adrianhopebailie](https://github.com/adrianhopebailie) |
| Ruby [Gem](https://rubygems.org/gems/investec_open_api) and [Wrapper](https://github.com/Offerzen/investec_open_api) | Ruby Gem and Wrapper for the Investec Open API. | *Ruby* | [@OfferZen](https://github.com/Offerzen) |
| [Elixir Package](https://hex.pm/packages/investec_open_api) | Elixir Package for the Investec Open API. It can also be used as a [Wrapper](https://gitlab.com/theodowling/elixir-investec-open-api) for the API. | *Elixir* | [@theodowling](https://gitlab.com/theodowling) |
| [Python CLI](https://github.com/banchee/investecli) | Python CLI Wrapper for Investec Private Bank Accounts. | *Python* | [@banshee](https://github.com/banchee) |


## Built with Investec Programmable Card

| Open Source Repo | Description | Tech Stack | Author |
| - | - | - | - |
| [Command center bridge](https://gitlab.com/fisher.adam.online/command-center-bridge) | A bridge solution for other programmable banking solutions that enables a single point of contact for approval determination and secure transaction forwarding to other APIs. Constructed in conjunction with [aws-cdk-js-dev-guide](https://github.com/therightstuff/aws-cdk-js-dev-guide).  | *Tools: CDK (a mix of TypeScript and Javascript) Components: AWS: api-gateway, lambda, sqs, dynamodb* | [@fisher.adam.online](https://gitlab.com/fisher.adam.online) |
| [Investec Query Bot](https://github.com/lebomorojele/investecbot)|A NLP service for interpreting transaction data from Investec Programmable Card API 🤖|*Python, Flask, SQLAlchemy, Rasa*|@lebo0|
| [pdb-investec-transaction-insights](https://gitlab.com/grimx13/pdb-investec-transaction-insights) | Store programmable card transactions in Google Sheet | *AWS Serverless, Google Sheets* | @grimx13 |
| [programmable-banking-glow](https://github.com/offerzen/programmable-banking-glow) | First iteration of a team spending dashboard. | *JS, Google Script* | @jethrof @jeriko1 |
| [InvestecPOC](https://gitlab.com/wernerpereira/InvestecPOC) | Store programmable card transactions in S3. Can be extended to trigger processing on object creation. | *C#*, *Swift* | @WernerPereira |
| [ur-command-center](https://gitlab.com/dale10/ur-command-center) | Infrastructure to save programmable card transactions into S3. | *C#*, *Swift* | @dale10 |
| [investec-logs-functions](https://github.com/JeremyWalters/investec-logs-functions) | Store programmable card transactions in Firebase. | *JS, Firebase* | [@JeremyWalters](https://github.com/JeremyWalters/) |
| [investec-logs-web](https://github.com/JeremyWalters/investec-logs-web) | A Vue frontend for Firebase (builds on [investec-logs-functions](https://github.com/JeremyWalters/investec-logs-functions)  | *Vue* | [@JeremyWalters](https://github.com/JeremyWalters/) |
| [investec-command-center](https://gitlab.com/bezchristo/investec-command-center) | Process investec programmable card transactions via GCP services  | *GCP, JS* | @bezchristo |
| [programmable-banking-rules](https://gitlab.com/HagashenNaidu/programmable-banking-rules) | A Blazor WebAssembly app that uses a Warewolf back-end to allow the customization of rules for accepting/declining transactions using Investec's Programmable Banking platform.  | *Blazor WebAssembly, Warewolf* | @HagashenNaidu |
| [ynab-sync](https://github.com/ferdis/ynab-sync) | Sync programmable banking card transactions with YNAB.  | *YNAB, Python.* | [@ferdis](https://github.com/ferdis) |
| [programmable-banking-power-bi-template](https://github.com/dalion619/programmable-banking-power-bi-template) | Sync programmable banking card transactions with PowerBI.  | *PowerBI* | @dalion619 |
| [programmable-banking-cf-worker](https://github.com/dalion619/programmable-banking-cf-worker) | Cloudflare worker.  | *Cloudflare* | @dalion619 |
| [Go Backend](https://github.com/donohutcheon/gowebserver)|Go backend for programmable card transactions management.|*Go*|@donohutcheon|
| [React Frontend](https://github.com/donohutcheon/reactwebclient)|React frontend for programmable card transactions management.|*React*|@donohutcheon|
| [Telegram to Wave Receipt Capture]((https://gitlab.com/benblaine/bill/))|A hack to remind you via Telegram message to send your receipt to Wave Receipts.|*Telegram Bot API, Wave Receipts, Email*|@benblaine|


## Demos

We meet every Thursday at 6:30pm over Zoom to see demos from community members. Come support us :)!

| <img src = "/images/youtube_logo.png"> Demo | Open Source Repo | Description | Tech Stack | Author |
| ------ | ------ | ------ | ------ | ------ |
|[Watch](https://youtu.be/xG_gqGwfnhQ) Consuming about GraphQL in the Browser | [<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community-Marcin-s-GraphQL-Wrapper-for-the-OpenAPI_Inner-Article-Image.png" width="200">](https://github.com/naartjie) | As a follow up to his presentation [GraphQL Wrapper for Investec open API](https://youtu.be/PmGy-2p3dwo), Marcin took us on a deep dive into the frontend developer experience. It was a really insightful session. If you want to learn more about the power of GraphQL and get some ideas of what you can do with the Investec Open API then you should check it out. Thanks Marcin!.| ** | [Marcin](https://github.com/naartjie) |
|[Watch](https://www.youtube.com/watch?v=ROzQkNj3xP8&ab_channel=OfferZen) or [Read](https://www.offerzen.com/blog/programmable-banking-community-renens-transaction-notifications) Transaction Event Listening | [<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community--Renen-s-Transaction-Notifications_Inner-Article-Image.png" width="200">]() | [Slide deck](https://drive.google.com/file/d/17_JJttJ5PYsrhw0wlltNx9Xl6Ah8RKL6/view) - Renen Watermeyer shows how he built a system that checks an account via the Investec OpenAPI to find new transactions every minute. He also demos how he created a webhook mechanism to push new notifications to another service.| ** | [Renen](https://github.com/sasa-solutions/investec_poller/) |
|[Watch](https://www.youtube.com/watch?v=g13lB10RFcw&list=PLjTry3duaTAGJ6UOW5ISS_ymV46-VqkPa&index=3&t=0s) or [Read](https://www.offerzen.com/blog/programmable-banking-community-lishens-temporary-store-of-value) about Temporary Store of Value | [<img src = "https://offerzen.ghost.io/content/images/2020/10/Investec-Programmable-Banking-Meetup----Lishen-s-Temporary-Store-of-Value_-Programmable-Banking-Community--Michael-s-Use-of-Protobuf-for-OpenAPI_Inner-Article-Image.png" width="200">](https://github.com/ihiresolutions?tab=repositories&q=investec&type=&language=) | [Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vSwtUBwBAUydVpNyERES8L70a5V8yt_Hibc-x3OSKxvHZUKA5ACAi9aBkpCBuqlSL7zuNeDR_HrUdtK/pub?start=false&loop=false&delayms=3000&slide=id.g92d0868e55_0_169) - Lishen Ramsudh discusses the tool he’s been working on to streamline business operations and open up product possibilities through the use of a temporary store of value. The temporary store of value and automation of certain operations opens up a different trust relationship with a process being the custodian instead of hoping that a transaction is upheld.| *C# DotNet Core 3.1, Postgres, Redis, Xamarin Forms, (eventually include Docker, AWS ECS Fargate and API Gateway, AWS Aurora)* | [Lishen](https://github.com/ihiresolutions) |
|[Watch](https://www.youtube.com/watch?v=B_7XU11TkzM) or [Read](https://www.offerzen.com/blog/programmable-banking-community-christos-crypto-hedge) about Crypto Hedge | [<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community--Christo-s-Crypto-Hedge_Inner-Article-Image.png" width="200">]() | [Slide deck](https://docs.google.com/presentation/d/10Oz005QN8axJvkCh2yEpjZrvFLhNRmSlCCEz8904vB4/edit) - Christo Bezuidenhout shares how he built a tool to hedge his spending against the Rand using crypto. His goal was to see if he could build something to 10X using programmable banking.| *Next.js, React, GCP (PubSub, Functions), Firestore, Auth0, Ant Design, Puppeteer.* | [Christo Bezuidenhout]() |
|[WATCH: GraphQL Wrapper for Investec open API](https://youtu.be/PmGy-2p3dwo) | [<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community-Marcin-s-GraphQL-Wrapper-for-the-OpenAPI_Inner-Article-Image.png" width="200">](https://github.com/naartjie/investec.graphql) | [Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vS_Hb3oZbY3-aXCugroKkyEbelPEL8ApLYsfXF4wZJbMBpvxYztHGWcOUvLQ7nwdXKdXEEgjEAjqc9o/pub?start=false&loop=false&delayms=3000) - Exploring functional languages to create a GraphQL wrapper for restful apis. This week he will take us through round 1, an F# implementation.| *F#, GraphQL* | [@naartjie](https://github.com/naartjie/investec.graphql) |
|[WATCH: OneCart - Fraud detection and prevention](https://www.youtube.com/watch?v=4DimmR1AkMc) | <img src = "https://offerzen.ghost.io/content/images/2020/09/Michael_Programmable-Banking-Community--Using-programmable-banking-for-fraud-detection-and-prevention_Inner-Article-Image.png"  width="200"> | [Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vTgs0DmoCq5n03x1nvoXOsfste4DYdVf8RpMO3tETBmP-LD5-JWG2xXgYB3hb9X_3JawTtULj2GYOuI/pub?start=false&loop=false&delayms=3000)] - Problem: OneCart has around 140+ shoppers working daily across SA. Each shopper has their own OneCart debit card and are fulfilling customer orders. They steal! Why solve this? Reduce shopper theft and be more proactive to shrinkage. Solution: Real-time alerts of suspicious activity on cards. | *Investec Programmable Card, Serverless, Slack webhooks, QlikView* | @michaellouis157 |
|[WATCH: Client Creator](https://www.youtube.com/watch?v=rcZTnfBQ7mA&t=1205s)] | [<img src = "https://offerzen.ghost.io/content/images/2020/10/_Programmable-Banking-Community--Michael-s-Use-of-Protobuf-for-OpenAPI_Inner-Article-Image@2x.png" width="200">](https://www.offerzen.com/blog/programmable-banking-community-michaels-client-creator) | [Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vRbvnrwKUAwXaEG80n3VQlahSDTzP1xqCfoOBsDjX0p2ZSrwTM4nv-eSm9v8ABTNQws-soQBRLPqtpb/pub?start=false&loop=false&delayms=3000) - How to use proto to create open banking clients.| *protobuff* | @mikewl |
|[WATCH: Receipt Capture App](https://www.youtube.com/watch?v=rcZTnfBQ7mA&t=0s) | [<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community--Loic-s-Receipt-Capture-App_Inner-Article-Image.png" width="200">](https://www.offerzen.com/blog/programmable-banking-community-loics-receipt-capture-app) | [Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vQL56yCJ2t11oqdpHMkL7847ddVa-6P4WBKb_Gpl2yEldxK8fAYZrV3k1-5uuUadoz16K-L8T-4o8Ps/pub?start=false&loop=false&delayms=3000)] | *Flutter, Flask, Firebase* | @LoicAn21 |
|[WATCH: O I CR What You *Almost* Did There](https://www.youtube.com/watch?v=vS9PcwWAAac&t=708s)| [<img src = "https://i.imgur.com/7W5VM4A.jpg" width="200">](https://gitlab.com/benblaine/bill/) |[Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vS47ev6jYFzupo1fHVCJjO6nsIj72PlKxnOWTqB6P9w94WP7DnwnvAyDRTRVEbb8entjdo7NBfZVAyd/pub?start=false&loop=false&delayms=3000) - What it does: Problem: I don’t know if I’m spending too much money on cheese - and the info is lost with the receipts I wantonly discard. Why solve this? Figure out if I’m “over-cheesing”. Solution: My Friend “Bill” who reminds me to capture my receipts..| *Telegram Bot API, Email, Wave* |*@benblaine*|
|[WATCH: Investec @ Home](https://www.youtube.com/watch?v=vS9PcwWAAac&t=1s)|[<img src = "https://offerzen.ghost.io/content/images/2020/11/Programmable-Banking-Community--Jo-rg-s-Smart-Home-Device-Extension_Inner-Article-Image.png" width="200">](https://www.offerzen.com/blog/programmable-banking-community-jorgs-smart-home-device-extension) |[Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vSB-UTh-O3BuSuyLuCzdvqE40DrlsjbW9_sCPC5GQrdC8fwKnLBB96nLiv1KTvmMWGfYYQcrIIdqIHt/pub?start=false&loop=false&delayms=3000) - A quick and dirty experiment to get my Investec account to interact with physical devices or a smart home.| *HomeBridge, Node.js, MQTT, HomeKit.* |*@avgjoe*|
|[WATCH: Investec Open Banking CLI](http://www.youtube.com/watch?v=kMdfLQ5fJwo&t=26m2s)| [<img src = "https://offerzen.ghost.io/content/images/2020/10/Programmable-Banking-Community--Adrian-s-Open-Banking-CLI_Inner-Article-Image@2x.png" width="200">](https://www.offerzen.com/blog/programmable-banking-community-adrians-open-banking-cli) |[Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vR8X1I48VzrgB9eJGTJEuy0H-Bm1ZH9PUcH1LQvZZ9lbxZSCYMPbU8-WMt_Qj9ZPlDOH2u3xDXLJLsk/pub?start=false&loop=false&delayms=3000) - What it does: CLI for working with your bank account(s). No need to log into app or Web. Built to learn Deno. How it works: Very simple (one Typescript file). REPL (Read Eval Print Loop). Basic state held in-memory and shown in prompt (e.g. :closed_lock_with_key: vs :unlock:). Logged in (valid access token). Selected Account. Privacy mode. Auto-login if possible (looks in ENV variables for credentials)..| *Deno* |*@adrianhopebailie*|
|[WATCH: Sync Investec to Sage One](http://www.youtube.com/watch?v=kMdfLQ5fJwo&t=0m1s)| [<img src = "https://offerzen.ghost.io/content/images/2020/09/Programmable-Banking-Community--Syncing-Investec-to-Sage-One_Inner-Article-Image.png" width="200">](https://www.offerzen.com/blog/syncing-transactions-into-sage-one-with-investec-programmable-banking) |[Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vQ7MlSACjBSTg0L3rQNOM0Un6IbFxtE-OOZ6MRmXqUwBrba7nrL1N6xSOIeS09HdUt3fv3St8y1xAoH/pub?start=false&loop=false&delayms=3000) - What it does: Syncing transactions automatically from Investec into Sage One. The current methods of importing transactions are manual or where you have to give your credentials to a third party. How it works: Retrieves the transactions from both Investec and Sage to compare. Removes transactions already synced. Sage will have the transaction hash. Compares manually added transactions in Sage One to stop duplication. Imports the transactions that are left over..| *FastAPI, Sage API, Python, Heroku, Python Investec OpenAPI Wrapper* |*@imraanp*|
|[WATCH: Flutterflys in Spring](https://www.youtube.com/watch?v=7Mr4E_w14sE&t=0s)| [SparaBox](https://github.com/RendaniDau/Sparabox) and [SparaBoxFlutter](https://github.com/RendaniDau/SparaBoxFlutter) <img src = "https://offerzen.ghost.io/content/images/2020/08/Programmable-Banking-Community--Rendani-s-Personal-Budget-App_Inner-Article-Image@2x.png" width="200"> |[Slide deck](https://docs.google.com/presentation/d/e/2PACX-1vS_soKErMuTtiW_zqRJopznnIadQONEXOkG5POxh0z8z_MjFLVkpuoyzUQL1NScShwH6K-F98yUzLMZ/pub?start=false&loop=false&delayms=3000) - Personal budget app to explore the potentials of programmable banking..| *Spring Boot, Keycloak, Flutter, Android* |*@rendani10*|
|[WATCH: OfferZen Finance App](https://www.youtube.com/watch?v=7Mr4E_w14sE&t=840s)| [<img src = "https://github.com/Offerzen/roz/raw/master/docs/simple-schema.jpg" width="200">](https://github.com/Offerzen/roz) |[Slide deck](https://docs.google.com/presentation/d/1Q7_8RGpXqA1F20enKxojbcM3avdW8Y6XmgBX3DgC7Yg/edit#slide=id.p) - Empowering the OfferZen team to spend money and helping the Finance team manage that spend easily and confidently.| *Ruby on Rails, PostgreSQL, Heroku* |*[@OfferZen](https://github.com/Offerzen/investec_open_api)*|
|[WATCH: Command center bridge](https://youtu.be/mszU70RzQWQ)| [<img src = "https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/raw/master/images/command-center-bridge-architecture.jpg" width="200">](https://gitlab.com/fisher.adam.online/command-center-bridge) | A bridge solution for other programmable banking solutions that enables a single point of contact for approval determination and secure transaction forwarding to other APIs. Constructed in conjunction with [aws-cdk-js-dev-guide](https://github.com/therightstuff/aws-cdk-js-dev-guide).| *Tools: CDK (a mix of TypeScript and Javascript) Components: AWS: api-gateway, lambda, sqs, dynamodb* |*[@fisher.adam.online](https://gitlab.com/fisher.adam.online/command-center-bridge)*|
|[WATCH: Investec CLI Tool](https://youtu.be/-HrEy_8OwgI?t=1116)| [REPO](https://github.com/banchee/investecli)| Get accounts, balance and transactions in your CLI.| *Python Tabulate, Python Fire (CLI)* |*[@rpurdon-nf](https://github.com/rpurdon-nf)*|
|[Shared Expenes](https://youtu.be/-HrEy_8OwgI)|  |[Slide deck](https://www.youtube.com/redirect?v=-HrEy_8OwgI&event=video_description&q=https%3A%2F%2Fdocs.google.com%2Fpresentation%2Fd%2F141EWHLj84RxiMXCSzgl6EkEUhGHDUrwrJARa6OeqCZ4%2Fedit%23slide%3Did.g7757fe7c3f_0_136&redir_token=QUFFLUhqbERBNW5TZS1Za1Z4UXdMaXk2Ni05bGpiaWVCUXxBQ3Jtc0tuUHAtNjd4S0xBb0RQaFM0M04wSTYxb1N2bHdfN2U3VERqVjRDMVl1UVJ2N3o2OGdoOEdBZmstR19xaDR0a3VvMFVncWhiWTZDc0lfUnpHbnFqdFlORlEyNklaellXUDNTT1phUF8tSTRFdVhjdWtOSQ%3D%3D) - Simple sharing of expenses in small groups. Existing shared expense app needed Investec Programmable Banking to make adding expense easier..| *Rails, React, Redux, Postgres* |*@jethrof*|
|[WATCH: Money Report Dashboard Mirror](https://youtu.be/F7qDvejZs9o)| [<img src = "/images/projectSystemDiagrams/pivendrenDashboard.png" width="200">](https://github.com/pivendren/openbanking-investec-dashboard) |[Slide deck](https://docs.google.com/presentation/d/1yP8lLos60CFcL3dhWuKQf_u6_FRtzdoGEA5iS1Xv3Qo/edit#slide=id.g7757fe7c3f_0_105) Component based dashboard configurable by user.| *Blazor WebAssembly, Entity Framework, .NET Core, ML.NET, SQL DB, Azure Functions* |*[@pivendren](https://github.com/pivendren)*|
|[WATCH: Vanilla Transaction Dashboard](https://youtu.be/2Z3IERMvXak)|[Go Backend](https://github.com/donohutcheon/gowebserver) AND [React Frontend](https://github.com/donohutcheon/reactwebclient)| [Slide deck](https://docs.google.com/presentation/d/1kBH-G9Zv5yPxfdevV6IXQ3Ithiss2aVihVOcM-cfdIc/edit#slide=id.g7757fe7c3f_0_105) - Why create it? Learn stuff: Primarily Go. But also React, Heroku. Build a dashboard to record my spending habits. Create a platform upon to build something cooler.  | *React, Go, Heroku* |@donohutcheon|
|[WATCH: Transacational Insights](https://www.youtube.com/watch?v=R0i_UntwiIE)|[programmable-banking-power-bi-template](https://github.com/dalion619/programmable-banking-power-bi-template) AND [investec-openbanking-dotnet](https://github.com/dalion619/investec-openbanking-dotnet) AND [programmable-banking-cf-worker](https://github.com/dalion619/programmable-banking-cf-worker)| [Slide deck](https://docs.google.com/presentation/d/1XBhGZWm9jbvhSrpZJhqLMb6cuycb8UOYG8UFxtlUN88/edit) Data mining bank account transactions for meaningful insight. Low code approach that non-devs can use. | *PowerBI, Cloudflare, Azure Functions, Investec Open API* |*[@dalion619](https://gitlab.com/https://github.com/dalion619/)*|
|[WATCH: Toshigotchi](https://www.youtube.com/watch?v=KEVJDwFvO_s)||[Slide deck](https://docs.google.com/presentation/d/1TmDkEkm6mM5yEW_A91EzmFRzayzupH9th3WGVQBLnIE/edit) - What it is: Create an emotional connection to your spending habits. Money is a very emotionally charged topic and there we should be ways to make it more personable & relatable so we can react better to various financial circumstances.||@jacousteau|
|[WATCH: Paper Budget](https://youtu.be/DKV-4M4a6CE?t=1)|[<img src = "https://i.imgur.com/o0s176D.png" width="200">](https://github.com/ferdis/ynab-sync)| [Slide deck](https://docs.google.com/presentation/d/11mcO96L9E6iWscG9ycO_Gy4wc90PY6E5N7SPrOjMhL8/edit#slide=id.g7757fe7c3f_0_105): What it does: Sync transactions to YNAB budget and categorize them. Why create it? Keep budget up to date without manual intervention. | *Starlette, React and SQLite* |@ferdis|
|[WATCH: Investec Query Bot](https://youtu.be/DKV-4M4a6CE?t=559) |[<img src = "https://github.com/lebomorojele/investecbot/raw/master/example.gif" width="200">](https://github.com/lebomorojele/investecbot)| What it does: It takes a user’s input, defines their intent, extracts queryable information, and generates a query to the DB. Eg. Show me all my transactions in Cape Town above 400 during June. Why create it? I was unsure of what to do with the card data, so figured why not let the user decide what they want to do with it. | *Swift UI (iOS), Ngrok, RASA NLP/U, SQLAlchemy, Python* |@lebo0|
|[WATCH: Programmable Banking Rule (PBR)](https://youtu.be/aqZotFl9fQw?t=1859)|[<img src = "https://i.imgur.com/rmifZuh.png" width="200">](https://gitlab.com/HagashenNaidu/programmable-banking-rules)| What it does: Allows the creation of rules that can be used at present for accepting/declining a transaction. Why create it?  Most banking apps only allow hard limits to be set on spend. The idea with this is to put the account holder in charge of the rules of the card.. | *.Net Core, Blazor WebAssembly, Warewolf, Sql DB* |@HagashenNaidu|
|[WATCH: InvestecCMD](https://youtu.be/aqZotFl9fQw?t=669)|| What it does: Sync transactions to Firestore as they come in. From Firestone, you can view you transactions in the iOS app as well as details of the transactions. You will also receive pushes when transactions are approved to the iOS App. Why create it? Great opportunity to learn how the Investec APIs works & to cut my teeth on a bunch of new techs I been dying to play with: firebase, cloud functions. typescript.. | *Native iOS, Swift, Firebase (Firestore, Cloud Functions)* |@WarrenFoxley|
|[WATCH: Decentralised API](https://youtu.be/LrnBHsY55uA?t=636)|| What it does: The goal was to create a decentralised API to handle incoming beforeTransaction and afterTransaction requests before storing the data. This allows you to execute logic prior to storing the data and before validating transactions. Why create it? The main goal was to allow for validation logic on a transaction based on externally defined criteria, eg: limit daily spend, limit per transaction spend etc. Also, this provided an authenticated basecamp on which to build logic. | *Google Cloud Platform, Laravel* |@cwbmuller|
|[WATCH: HankApp](https://youtu.be/3tYfpc_rjvU?t=1202)|| Hank is banking done right. Imagine a world where banking is an intuitive pleasure. It's our mission to make that world a reality. As banks open up their API's we aim to unlock their utility for the people. | *Flutter, Firebase* |*[@Chris.Fraser](https://gitlab.com/Chris.Fraser)*|
|[WATCH: Investec Oracle](https://youtu.be/tnpsjOFbkco?t=295)|[<img src = "https://gitlab.com/bezchristo/investec-command-center/-/raw/master/Investec.jpg" width="200">](https://gitlab.com/bezchristo/investec-command-center)| What it does: Routes data through cloud functions to Google pub/sub, to which I can add multiple subscriptions for different applications. Why create it? To serve as a base for all transactions to flow through and to which different applications can connect to to receive transaction data. I also wanted to have the ability to replay transactions, which Google pub/sub provides and make it easy for other users to be able to spin up their own command center with a few clicks.  | *Google Cloud Platform (Cloud functions, pub/sub, Firestore), Twillio, React, Zapper* |*[@bezchristo](https://gitlab.com/bezchristo)*|
|[WATCH: Purple Pigger](https://youtu.be/5xiRrJbblc8?t=2841)|| Linked to the beforeTransaction function, PP checks all transactions (for now) and approves or declines them, to help you control budgets in certain periods. Keeping those wild nights, not too wild. I keep a mental count of my daily spending and how it affects my budget. Now I can get a machine to do it. Basically I'm lazy!. | *Firebase, Svelte* |*[@kamogelo.sebopelo](https://gitlab.com/kamogelo.sebopelo)*|
|[WATCH: Payments Splitter](https://youtu.be/qkFFE3aIGZc)|| An app that let's you split the bill for a transaction with a friend using their email address. | *Node, AWS* |*[@Yashu Tanna](yashutanna @yashutanna)*|
|[WATCH: Investec Logs](https://youtu.be/Hkv8qeScXQ0)|[investec-logs-functions](https://github.com/JeremyWalters/investec-logs-functions) AND [investec-logs-web](https://github.com/JeremyWalters/investec-logs-web)| System to log transactions to Google Firebase using NodeJS and a frontend view with VueJS. | *Firebase* |@JeremyWalters|
|[WATCH: Ur Command Center](https://youtu.be/addbahzP1ks)|[<img src = "https://gitlab.com/dale10/ur-command-center/-/raw/master/assets/CCArchitecture.png" width="200">](https://gitlab.com/dale10/ur-command-center)| Infrastructure to save transactions into S3. The idea is that S3 is the source of truth and this can be extended to trigger processing on object creation. That will come in a future iteration. | *AWS*, *Terraform*, *Lambda*, *S3*, *Grafana* |*[Dale Tristram @dale10](https://gitlab.com/dale10)*|
|[WATCH: InvestecPOC](https://youtu.be/3HDNL5CVwDs)|[<img src = "https://gitlab.com/wernerpereira/InvestecPOC/-/raw/master/main_architechture.png" width="200">](https://gitlab.com/wernerpereira/InvestecPOC)| Mobile App to categorise expenses. | *C#*, *Swift* |@WernerPereira|
|[WATCH: Receipt Scanner](https://youtu.be/0yVRTSd19Ac)|| Simple app to fire off messages from Go through Firebase Cloud Messenger. | *Go, Firebase Cloud Messaging* |*[@brian.entersekt](https://gitlab.com/brian.entersekt)*|
|[WATCH: Transappenate](https://youtu.be/PAgcFV0wHjc)|| A mobile app for storing and cateogrising transactions. | *Appenate* |*[@matthew.alport](https://gitlab.com/matthew.alport)*|
|[WATCH: Singularity](https://youtu.be/N6M4tE_KhIk)||Info management app, transaction logger, auto-create transaction with Investec Beta, charts to display spending. Third party lib: "QtWebApp HTTP Server in C++". | *C++* |*[@lcoetzer](https://gitlab.com/lcoetzer)*|
|[OfferZen's Spend Tracker](https://youtu.be/1dGC5E8GGQM)|[JS and Google Script Repo](https://github.com/offerzen/programmable-banking-glow)| This repo contains source code for our first iteration of a team spending dashboard, as demonstrated in the community update on 19 March 2020. | *JavaScript* |@jethrof @jeriko1|
|[WATCH: PDB Investec Transaction Insights](https://youtu.be/gAiP--sk4U8)|[<img src = "https://gitlab.com/grimx13/pdb-investec-transaction-insights/-/raw/master/images/investec_logger.png" width="200">](https://gitlab.com/grimx13/pdb-investec-transaction-insights)| This project utilises AWS Serverless and managed infrastructure to deliver transaction data to a Google sheet where I can draw insights from in near real time | *NodeJS*, *Python* | @grimx13 |
|[demo](youtubelink)| [<img src = "/imagelink.png" width="200">](repolink) |[Slide deck](slidesURL) - description.| *Tech_Tools* |*[@author](https://gitlab.com/author)*|

## Some Useful Resources

### Auth

- [Cloudflare Auth0](https://developers.cloudflare.com/workers/tutorials/user-auth-with-auth0)
- [auth0.com](auth0.com)

### AWS CDK

- [aws-cdk-js-dev-guide](https://github.com/therightstuff/aws-cdk-js-dev-guide)

### CI/CD

- [GitLab CI/CD | GitLab](https://docs.gitlab.com/ee/ci/)
- [Github actions](https://github.com/features/actions)

### Prototyping tools
- [ngrok - secure introspectable tunnels to localhost](https://ngrok.com/)

### Web App
- [Ruby on Rails | A web-application framework](https://rubyonrails.org/)

### Database / Storage

- [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine)
- [InfluxDB](https://www.influxdata.com/)
- [RethinkDB](https://rethinkdb.com/)

### Front-end
- [VueJS](https://vuejs.org/)
- [ReactJS](https://reactjs.org/)

### Chat/Chatbots

#### Chat Interfaces

- [Telegram Bots: An introduction for developers](https://core.telegram.org/bots)
- [Facebook Messenger: Chatfuel](https://chatfuel.com/)
- [Slack Bots API](https://api.slack.com/bot-users)

#### Dialogflow - NLP

If you want to create chat logic so that you can interact with your chatbot via text, this is a great platform.

- [Basic Tutorial](https://developers.google.com/actions/dialogflow/first-app) 
- [Website](https://dialogflow.com/) 
- [Sample Apps](https://dialogflow.com/docs/examples/) 
- [Starter example with API integration](https://dialogflow.com/docs/getting-started/basic-fulfillment-conversation) 
- [Crafting a Conversation](https://developers.google.com/actions/design/walkthrough#write_dialogs)

#### Other NLP Solutions
- [Wit.ai](https://wit.ai/) 
- [Rasa](http://rasa.com/) 
