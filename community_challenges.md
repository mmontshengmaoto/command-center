# Community Challenges
This page is about how community challenges work. See the [challenges board](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/boards/1927995) for submitted and in progress challenges.

The intention with community challenges is to incentivise the creation of "building blocks" in the form of reusable libraries, utilities and code snippets. This should reduce the effort required for others in the community to build personal and business projects.

Challenge acceptors create open source software by completing community challenges, get featured on our Gitlab and Youtube and win cool prizes 👾 💪🏆 

*Contact Ben on [email](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/issues/new) or [Slack](https://app.slack.com/client/T8CRG18UC/D8D0JTUBE) if you have questions.*

## Submit a challenge

**To submit a new challenge**: Add an issue to the "Submitted Challenges" list on the [Command Center Issues Board](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/boards/1927995). Use [this template](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/issues/30).

## Accept a challenge

1. **Accepting a challenge:** Assign yourself to an issue on the [Command Center Issues Board](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/boards/1927995). Move the issue into the `Accepted Challenges` list.
2. **Build and publish code:** Build a solution, submit your code repo and get featured on the community Gitlab.
3. **Demo your solution:** Book a demo at the weekly meetup and get featured on the community Youtube channel.
4. **Claim a reward:** We will reward you with one of the prizes from The Bounty Box.

## Completed challenges 🚀

[See community projects.](https://gitlab.com/offerzen-beta-community/investec-programmable-banking/command-center/-/blob/master/README.md)

## The Bounty Box 🏆

| Bounty | Description |
| ------ | ------ |
|![](/images/bounties/offerzenswapgpack.png)|OfferZen Swag Pack|
|![](/images/bounties/djitello.jpg)|[DJI Tello Programmable Drone](https://www.youtube.com/watch?v=_v_RknPrebI). This prize is for epic challenges only.|
|![](/images/bounties/sphero.jpg)|[Sphero SPRK+ Programmer Robot](https://www.youtube.com/watch?v=Yg8LmEkI_0c). This prize is for epic challenges only.|
|![](/images/bounties/dell27.jpeg)![](/images/bounties/pi4.jpg)|[Magic Mirror Kit: Raspberry Pi 4 Model B 8GB and Dell 27" monitor](https://www.youtube.com/watch?v=npzRf5wuIB0). This prize is for the Magic Mirror challenge only.|


## FAQs
**Can multiple people/teams work on the same challenge?**
Generally a challenge will be awarded to one person or team at a time, but that doesn't stop teams from combining forces. We will try keep it to one solution per challenge though.

**What is an epic challenge?**
An `epic challenge` is harder and therefore earns you the option to win epic challenge prizes. A `special challenge` unlocks specific/special prizes. See prize descriptions for epic and special prizes.

**May I work in a team on a challenge?**
Yes, challenge acceptors may work in teams. You will need to share prizes.

**How long do I have to complete a challenge?**
Your team has 14 days to complete the challenge.

**How is the quality of the code evaluated?**
At a minimum your code must run and be reusable by another member of the community. You must submit simple steps to set up your code. We'll be improving the evaluation criteria over time.
