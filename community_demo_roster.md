**Waiting list**
- Nick Corin RE Go Wrapper
- adrianhopebailie RE working at Ripple
- Edward van Kuik RE LawPracticeZA integration
- Kevin Boshoff RE how African Alliance integrates
- Hackathon teams RE building their solutions
- VAT IT expense tracking

|Upcoming Demos|Presenters|
|-|-|
|||

|Past Demos|Presenters|
|-|-|
|Thu 9 Apr|[Matthew Alport @matthew.alport](https://gitlab.com/matthew.alport), [Brian Machimbira @brian.entersekt](https://gitlab.com/brian.entersekt)|
|Thu 16 Apr|[Werner Pereira @wernerpereira](https://gitlab.com/wernerpereira), [Dale Tristram @dale10](https://gitlab.com/dale10)|
|Thu 23 Apr|[Junior Walters @walterjc21](https://gitlab.com/@walterjc21), [Dave Saunders]()|
|Thu 30 Apr|[Yashu Tanna @yashutanna](https://gitlab.com/yashutanna), |
|Thu 07 May|[@ADD_PLAYER](https://gitlab.com/), [Kamogelo Sebopelo @kamogelo.sebopelo](https://gitlab.com/kamogelo.sebopelo)|
|Thu 14 May|[Christo Bezuidenhout @bezchristo](https://gitlab.com/bezchristo), |
|Thu 21 May|[Grant Lewis @grant29](https://gitlab.com/grant29)|
|Thu 28 May|[Chris Muller @cwbmuller](https://gitlab.com/cwbmuller)|
|Thu 04 Jun|[Warren Foxley @WarrenFoxley](https://gitlab.com/WarrenFoxley), [Hagashen Naidu @HagashenNaidu](https://gitlab.com/HagashenNaidu)|
|Thu 11 Jun|[Daniel Davey @jeriko1](https://gitlab.com/jeriko1)|
|Thu 18 Jun|[Lebo Morojele @lebo0](https://gitlab.com/lebo0), [Ferdi Schmidt @ferdis](https://gitlab.comferdis)|
|Thu 25 Jun|[Jacques Coetsee @jacousteau](https://gitlab.com/jacousteau)|
|Thu 02 Jul|@dalion619|
|Thu 09 Jul|@donohutcheon|
|Thu 16 Jul|@pivendren|
|Thu 23 Jul|@jethrof, Ross Purdon|
|Thu 30 Jul|@fisher.adam.online, |
|Thu 06 Aug|OfferZen Team, @rendani10|
|Thu 13 Aug|@imraanp, @adrianhopebailie|
|Thu 20 Aug|@avgjoe , @benblaine|
|Thu 27 Aug|@LoicAn21 , @mikewl|
|Thu 03 Sep|@lesibar, @michaellouis157|
|Thu 10 Sep|@naartjie|
|Thu 17 Sep|@kamogelo.sebopelo|
|Thu 1 Oct|@renenw, @bezchristo|
|Thu 8 Oct|@ihiresolutions, Investec Team|
|Thu 15 Oct|@renenw|
|Thu 22 Oct|@naartjie|
|Thu 29 Oct|Workshop|